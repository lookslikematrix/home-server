# nfs-subdir-external-provisioner

~~~bash
git clone https://gitlab.com/lookslikematrix/home-server.git
cd home-server/nfs-subdir-external-provisioner
cp values.yaml values.local.yaml
~~~

~~~bash
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
helm repo update
helm upgrade nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --namespace nfs-subdir-external-provisioner --install --create-namespace --version 4.0.16 -f values.local.yaml
~~~