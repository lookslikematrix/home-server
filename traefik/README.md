# traefik

You need to install your own *traefik* service because the default *traefik*-service is disabled because *k3s* includes an old version.

~~~bash
git clone https://gitlab.com/lookslikematrix/home-server.git
cd home-server/traefik
cp values.yaml values.local.yaml
~~~

In the *values.yaml* file it is configured that HTTP is always redirected to HTTPS.

~~~bash
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
helm upgrade traefik traefik/traefik --namespace traefik --install --create-namespace --version v10.24.0 -f values.local.yaml
~~~

## Update Customer Resource Defintions (CRD) from traefik 2.4 to higher versions

CRD are not update with *helm* so we have to update this on our own.

~~~bash
./update_crd.sh
~~~

I found this here:
[https://gist.github.com/Startouf/bd961cee307a6ad93aba51f082f4b7f6](https://gist.github.com/Startouf/bd961cee307a6ad93aba51f082f4b7f6)
