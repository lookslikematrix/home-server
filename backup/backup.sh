#!/bin/sh

PIDFILE=/root/workspace/backup/backup.pid
server="serveraddress"
server_user="pi"
port="22"
server_dir="/media/ext/data"
client_dir="/mnt/data"
backup_log_dir="/root/log"

# Define a timestamp function
timestamp() {
  date +"%Y-%m-%d_%H-%M-%S"
}

# backup all persitant volume claims
backup_pvcs() {
  echo "PVCs BACKUP" >> $logfile
  rsync -a --delete -v -e "ssh -p $port" $client_dir/ $server_user@$server:$server_dir/pvcs/ >> $logfile
  echo $'\n' >> $logfile
}

# create Log-File
logfile="$backup_log_dir/$(timestamp).log"
touch $logfile

# create PID file to only execute script once
if [ -f $PIDFILE ]
then
  PID=$(cat $PIDFILE)
  ps -p $PID > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
    echo 'Process already running' >> $logfile
    exit 1
  else
    ## could not find process
    echo $$ > $PIDFILE
    if [ $? -ne 0 ]
    then
      echo 'Could not create PID file' >> $logfile
      exit 1
    fi
  fi
else
  echo $$ > $PIDFILE
  if [ $? -ne 0 ]
  then
    echo 'Could not create PID file' >> $logfile
    exit 1
  fi
fi

if [ -z "$1" ]
  then
    echo 'Backup all'
    backup_pvcs
  else
    echo 'Backup only '$1
    case "$1" in
      pvcs) backup_pvcs
      ;;
      *) echo 'No backup!'
      ;;
    esac
fi

rm $PIDFILE