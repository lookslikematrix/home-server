# OpenHAB

When you like to install OpenHAB at the moment you have to do the following steps:

~~~bash
git clone https://gitlab.com/lookslikematrix/home-server.git
cd home-server/openhab
cp values.yaml values.local.yaml
~~~

Make changes to the *values.local.yaml* file in the *openhab*-directory, create the package and install or upgrade it.

~~~bash
helm package .
helm upgrade openhab openhab-0.3.tgz --namespace openhab --install --create-namespace -f values.local.yaml
~~~

If you like to access the **Karaf Console** you have to make the following changes in your persistent volume *openhab-pvc-conf*. Access your **OpenHAB** pod, open *vi /openhab/conf/services/runtime.cfg*, make the change and save the file (*ESC* -> :wq -> *ENTER*).

~~~diff
-#org.apache.karaf.shell:sshHost = 0.0.0.0
+org.apache.karaf.shell:sshHost = 0.0.0.0
~~~

Go to your Pi-hole and add your OpenHAB URL (default: openhab.lookslikematrix) to the local DNS (Local DNS -> DNS Records) whit the external IP address of the cluster.

I will create a helm repository for this soon.