# Gitea

When you like to install Gitea at the moment you have to do the following steps:

* Change the default SSH Port on your Kubernetes Node if you like to use Gogs with SSH on the default port. Now you have to use `ssh user@lookslikematrix -p 25291` to connect directly to your node via SSH.

    ~~~bash
    sed -i 's/#Port 22/Port 25291/g' /etc/ssh/sshd_config
    systemctl restart sshd
    ~~~

* Download the repository

  ~~~bash
  git clone https://gitlab.com/lookslikematrix/home-server.git
  cd home-server/gitea
  cp values.yaml values.local.yaml
  ~~~

* Make changes to the *values.local.yaml* file in the *gitea*-directory, create the package and install or upgrade it. Especially you have to change the **ipAddress** value, so the *SSH* Port will be exposed on the right address.

  ~~~bash
  helm package .
  helm upgrade gitea gitea-0.2.2.tgz --namespace gitea --install --create-namespace -f values.local.yaml
  ~~~

* Go to your Pi-hole and add your gitea URL (default: gitea.lookslikematrix) to the local DNS (Local DNS -> DNS Records) whit the external IP address of the cluster.

I will create a helm repository for this soon.