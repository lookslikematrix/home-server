# cert-manager

~~~bash
git clone https://gitlab.com/lookslikematrix/home-server.git
cd home-server/cert-manager
cp values.yaml values.local.yaml
~~~

~~~bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm upgrade cert-manager jetstack/cert-manager --namespace cert-manager --install --create-namespace --version v1.8.2 -f values.local.yaml
~~~