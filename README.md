# home server

I'm starting to run my home server where everything is running on a single machine to a kubernetes cluster. I'll desribe here everything as detailed as I can.

My goal is that the following applications will run on my cluster and that you can run this home server as well.

* [x] [Pi-hole](./pihole/README.md)
* [x] [Radicale](./radicale/README.md)
* [x] [gitea](./gitea/README.md)
* [x] [OpenHAB](./openhab/README.md)
* [ ] Velero
* [ ] Seafile
* [ ] drone.io
* [ ] homematic

Fell free to contribute.

# Component Versions

|Component                                                                                            |Version     |Chart Version|
|-----------------------------------------------------------------------------------------------------|------------|-------------|
|[k3s](https://k3s.io/)                                                                               |v1.26.4+k3s1|-            |
|[traefik](https://github.com/traefik/traefik-helm-chart)                                             |2.8.0       |v10.24.0     |
|[nfs-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)|4.0.2       |4.0.16       |
|[cert-manager](https://artifacthub.io/packages/search?org=cert-manager)                              |v1.8.2      |1.8.2        |
|[Pi-hole](https://pi-hole.net/)                                                                      |2022.11.2   |0.9          |
|[Radicale](https://radicale.org/)                                                                    |3.1.7       |0.1.3        |
|[gitea](https://gitea.io/)                                                                           |1.16.9      |0.2.2        |
|[OpenHAB](https://www.openhab.org/)                                                                  |3.3.0-alpine|0.3.0        |

## Dependencies

* Install *k3s* (v1.26.4+k3s1) on your server. You can check [here](https://update.k3s.io/v1-release/channels) if there is a newer stable version.

    ~~~bash
    # switch to root user
    su
    apt install curl
    curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.26.4+k3s1 INSTALL_K3S_EXEC="--disable=traefik" sh -
    ~~~

* Copy *k3s.yaml* file locally (Debian/Ubuntu)
    
    ~~~bash
    scp /etc/rancher/k3s/k3s.yaml lookslikematrix@lookslikematrix-desktop:/home/lookslikematrix/.kube/config
    ~~~

* Install *kubectl* locally (Debian/Ubuntu)

    ~~~bash
    sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
    sudo apt-get update
    sudo apt-get install -y kubectl
    ~~~

* Change IP-Address in *config*-file
    
    ~~~bash
    sed -i 's/127.0.0.1/<SERVER_IP_ADDRESS>/g' ~/.kube/config
    ~~~

* Install *helm* locally (Debian/Ubuntu)

    ~~~bash
    curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
    sudo apt-get install apt-transport-https --yes
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install helm
    ~~~

* Install [traefik](https://github.com/traefik/traefik-helm-chart) in version 2.8.0 (Chart version v10.24.0)

    ~~~
    cd traefik
    ~~~

    Follow instructions in the [README](./traefik/README.md) there.

## How to create a NFS server with ZFS for our cluster

* When you use Debian 10 (Buster) add buster-backports as package source to your */etc/apt/sources.list* when it's not already there. On Debian 11 (Bullseye) just ignore this.

    ~~~bash
    deb http://deb.debian.org/debian buster-backports main contrib non-free
    ~~~

* Install *zfsutils-linux* on your raspberry pi. Just delete *-t buster-backports* if your on Debian 11.

    ~~~bash
    # change to root user
    su
    apt update
    apt install linux-headers-`uname -r`
    apt install -t buster-backports zfsutils-linux
    # press OK when there is a dialog about licenses
    # it take a few minutes ~10min
    ~~~

* Make sure the *ZFS*-service is running correctly. Otherwise restart the service (*systemctl restart zfs\**) or restart your pi (*reboot*).

* Discover your device ID. Just execute the next command, before and after inserting your device and in the output of the *diff* command you will find out the device ID.

    ~~~bash
    ls /dev/disk/by-id > /tmp/before_mount
    # insert storage
    ls /dev/disk/by-id > /tmp/after_mount
    diff /tmp/before_mount /tmp/after_mount
    ~~~

* Create a directory with full access

    ~~~bash
    mkdir -p /mnt/data
    chmod 777 /mnt/data
    ~~~

* Create a *ZFS* pool and mount your device by inserting the ID (e.g. *ata-SAMSUNG_HD103SI_S1VSJ90Z491353*)

    ~~~bash
    # Create pool
    zpool create tank ID
    # Mount tank
    zfs create -o mountpoint=/mnt/data tank/data
    ~~~

* Install *NFS* an share the *ZFS* pool. Insert your IP-Address range below.

    ~~~bash
    # NFS
    apt install nfs-kernel-server
    ~~~

* Make the following changes to */etc/default/nfs-kernel-server* and comment this configuration 

    ~~~diff
    - RPCMOUNTDOPTS="--manage-gids"
    + #RPCMOUNTDOPTS="--manage-gids"
    ~~~

* Share *ZFS* pool via *NFS*

    ~~~bash
    zfs set xattr=sa dnodesize=auto tank/data
    zfs set sharenfs="fsid=0,rw=192.168.178.0/24,no_subtree_check,no_root_squash" tank/data
    zfs share tank/data
    ~~~

* Install [nfs-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner) in version 4.0.2 (Chart version 4.0.16)

    ~~~
    cd nfs-subdir-external-provisioner
    ~~~

    Follow instructions in the [README](./nfs-subdir-external-provisioner/README.md) there.

* Change in every service your storage settings to the following

    ~~~yaml
    storage:
      class: nfs-client
      accessModes: ReadWriteMany
    ~~~

## How to backup

I wrote a little backup script to backup the *NFS*-Server content to another raspberry pi. Because at the moment this raspberry pi is not a 64-Bit-System I can't use *ZFS*-snapshots to backup the system. But here are the needed steps.

* First of all make sure you have access to the other raspberry pi via *SSH*

* After that install *rsync* on the raspberry pi where you run your *NFS*-server and set your timezone. (In my case Europe/Berlin)

    ~~~bash
    apt install rsync
    timedatectl list-timezones
    timedatectl set-timezone Europe/Berlin
    ~~~

* Copy the script from *backup/backup.sh* to your *NFS*-server and give the script executing rights.

    ~~~bash
    chmod +x backup/backup.sh
    ~~~

* Insert your configuration at the top of the script file

* Execute the script via *cronjob*. Run *crontab -e* and insert the following line. My backup runs always at 9 o'clock.

    ~~~bash
    0 9 * * * /root/workspace/backup/backup.sh
    ~~~

## How to create certificates

If you like you can protect your home server via HTTPS. Therefore you have to create a root certificate and for every service a extra certificate. Than you have to add your root certificate to your browser and operating system and you will have a secure connection.

On a Debian system you can create your certificates with *openssl*.

* Create a root certificate and remember the password because you will need it for every creation of a new certificate.

    ~~~bash
    openssl genrsa -out rootkey.pem 4096
    openssl req -x509 -new -nodes -extensions v3_ca -key rootkey.pem -days 1024 -out rootcertificate.pem -sha512
    ~~~


* Install [*cert-manager*](https://cert-manager.io/) in version v1.8.2 (Chart version 1.8.2)

    ~~~
    cd cert-manager
    ~~~

    Follow instructions in the [README](./cert-manager/README.md) there.

* Copy file

    ~~~bash
    cp cert-manager/cert-manager-secret.yaml cert-manager/cert-manager-secret.local.yaml
    ~~~

* Create a file with the base64 string of the *rootkey.pem* and the *rootcertificate.pam*. You can find this file also under *cert-manager/cert-manager-secret.yaml*

    ~~~yaml
    apiVersion: v1
    kind: Secret
    metadata:
        name: ca-key-pair
        namespace: cert-manager
    data:
        tls.crt: <insert output from `base64 rootcertificate.pem -w0`>
        tls.key: <insert output from `base64 rootkey.pem -w0`>
    ~~~

* Add secret to your cluster

    ~~~bash
    kubectl apply -f cert-manager/cert-manager-secret.local.yaml
    ~~~

* Create a CA cluster issuer

    ~~~bash
    kubectl apply -f cert-manager/ca-issuer.yaml
    ~~~

* Enable TLS in the service value files

## How to add a worker node to our cluster

* Install Raspberry Pi OS 64 Bit on another Raspberry Pi (I use a Raspberry Pi 3) [https://www.raspberrypi.com/software/operating-systems/](https://www.raspberrypi.com/software/operating-systems/)

* Add `cgroup_memory=1 cgroup_enable=memory` to */boot/cmdline.txt*. It should looks something like that.

    ~~~txt
    console=serial0,115200 console=tty1 root=PARTUUID=c2c0fdd4-02 rootfstype=ext4 fsck.repair=yes rootwait cgroup_memory=1 cgroup_enable=memory
    ~~~

* Get the **K3S_URL** from the main node which is basically the IP Address with *https* and *6443*

    * K3S_URL https://192.168.178.10:6443

* Get the **K3S_TOKEN** from the main node by executing the following on the main node

    ~~~bash
    cat /var/lib/rancher/k3s/server/node-token
    ~~~

    * K3S_TOKEN LONGTOKENWHICHSHOULDBESUPERSECRET

* Install k3s (v1.26.4+k3s1) on the worker node

    ~~~bash
    curl -sfL https://get.k3s.io | sudo K3S_URL=https://192.168.178.10:6443 K3S_TOKEN=LONGTOKENWHICHSHOULDBESUPERSECRET INSTALL_K3S_VERSION=v1.26.4+k3s1 sh -
    ~~~